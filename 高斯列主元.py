import numpy as np
def Gauss (A,b) :
    n=len (b) ;index=1;x = np.zeros (n)
    for k in range (n) :
        #＇选主元＇
        a_max=0
        for i in range (k,n) :
            if abs (A [i] [k]) > a_max:
                a_max = abs (A [i] [k])
                r=i
            if a_max < 0.00000001:
                index=0
                return
                #＇交换两行＇
            if r> k:
                for j in range (k,n) :
                    z = A [k] [j]; A[k][j] = A[r][j]; A[r][j] = z
                z = b [k] ;b [k] = b[r]; b[r] = z;
                #＂消元计算＂
            for i in range (k+1,n) :
                m=A [i] [k]/ A[k][k]
                for j in range (k+1,n) :
                    A [i] [j] = A[i][j] - m* A[k][j]
                b [i] =b [i] -m* b[k]
            #回代过程
            if abs (A [n-1] [n-1]) < 0.0000001:
                index=0
                return
            for k in range (n-1, 0-1, -1) :
                for j in range (k+1,n) :b [k] = b[k] - A[k][j]*x[j]
                x [k] =b [k] /A [k] [k]
                return index,x
A= [[15,2,3,2],[3,15,6,5],[5,6,18,3],[6,5,4,13]]
b= [22,28,13,15]
print ("高斯列主元：{}".format(Gauss (A,b) ))