# -*- coding: utf-8 -*-

# -- Sheet --

import math
f = lambda x: x**2-3*x+2-math.e**x
fd = lambda x: 2*x-3-math.e**x

#迭代法
def b(f, a, b, ep):
    fa = f(a)
    fb = f(b)

    if fa * fb > 0:
        return 0
    k = 1
    while abs(b - a) / 2 > ep:
        x = (a + b) / 2
        fx = f(x)
        if fx==0:
            return 1,k,x
        elif fx * fa < 0:
            fb = fx
            b = x
        else:
            fa = fx
            a = x
        k += 1

    x = (a + b) / 2
    return 1, k, x

#递归法
def b1(f,a,b,ep,k):
    fa = f(a)
    fb = f(b)
    k=k+1
    if fa * fb > 0:
        return 0

    x = (a + b) / 2
    fx = f(x)
    if(f(x)==0):
        return 1,k,x
    elif fx * fa < 0:
        return b1(f,a,x,ep,k)
    elif abs(b - a) / 2 < ep:
        return 1,k,x
    else:
        return b1(f,x,b,ep,k)
        

def n(f,fd,x,ep,it_max): 
    index=0 
    k=1 
    while k<it_max: 
        x1=x 
        fv=f(x) 
        fdv=fd(x) 
        if abs(fdv)<ep: 
            break 
        x=x1-fv/fdv 
        if abs(x-x1)<ep: 
            index=1 
            break 
        k+=1 
    return index,k,x

ans_b=b(f,0,1,10**-6)
ans_b1=b1(f,0,1,10**-6,0)
ans_n=n(f,fd,1.5,10**-6,20)
print(ans_b)
print(ans_b1)
print((ans_n))



