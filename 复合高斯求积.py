# -*- coding: utf-8 -*-
"""
Created on Fri Apr  1 08:40:03 2022

@author: Administrator
"""

from math import exp
from math import sqrt
from math import log
f = lambda x : 1/x

def G_quad(f, a, b, N):
    h = (b-a)/N;

    t = [-sqrt(3/5),0,sqrt(3/5)]
    A = [5/9,8/9,5/9]
    x_fromt = [0,0,0]
    Sum_G = 0
    for k in range(N):
        for i in range(3):
            x_fromt[i] = h/2*t[i] + a + (k+1/2)*h
        Sum_G = Sum_G + h/2 * (A[0]*f(x_fromt[0])+A[1]*f(x_fromt[1])+A[2]*f(x_fromt[2]))
    return Sum_G
quadG = G_quad(f,1,5,15)

print(quadG)
print(log(5)-quadG)
