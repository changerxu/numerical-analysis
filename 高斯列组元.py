# -*- coding: utf-8 -*-

# -- Sheet --

import numpy as np

def Gauss_CPE(A,b):
    n=len(b);index=1;x=np.zeros(n)
    for k in range(n):
        a_max=0
        for i in range(k,n):
            if abs(A[i][k])>a_max:
                a_max = abs(A[i][k])
                r=i
        if a_max<0.00000001:
            index=0
            return
        if r>k:
            for j in range(k,n):
                z=A[k][j];A[k][j]=A[r][j];A[r][j]=z
            z=b[k];b[k]=b[r];b[r]=z;
        for i in range(k+1,n):
            m=A[i][k]/A[k][k]
            for j in range(k+1,n):
                A[i][j]=A[i][j]-m*A[k][j]
            b[i]=b[i]-m*b[k]
    if abs(A[n-1][n-1])<0.0000001:
        index=0
        return
    for k in range(n-1,0-1,-1):
        for j in range(k+1,n):
            b[k]=b[k]-A[k][j]*x[j]
        x[k]=b[k]/A[k][k]
    return index, x
A=[[1,2,3],[4,5,6],[7,8,0]]
b=[2,3,1]
print(Gauss_CPE(A,b))

