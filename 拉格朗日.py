import math

def lagelangri(x,y,ans_x):
    ans_y=0
    for i_y in range(len(y)):
        temp=1
        for i_x in range(len(x)):
            if i_x!=i_y :
                temp*=(ans_x-x[i_x])/(x[i_y]-x[i_x])
        ans_y+=y[i_y]*temp
    return ans_y

x=[1,4,9,16]
y=[1,2,3,4]
ans_x=10


print(lagelangri(x,y,ans_x))


