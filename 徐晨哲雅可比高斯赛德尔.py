# -*- coding: utf-8 -*-

# -- Sheet --

import numpy as np

def Jacobi(A,b,x0,it_max,ep):
    D=np.diag(np.diag(A))
    U=-np.triu(A,1)
    L=-np.tril(A,-1)
    B=np.dot (np.linalg.inv (D),(L+U))
    f=np.dot (np.linalg.inv(D),b)
    x=np.dot(B,x0)+f
    k=1
    index=1
    while np.linalg.norm(x-x0)>=ep:
        x=x0
        x=np.dot(B,x0)+f
        k=k+1
        if k>it_max:
            index=0
            break
        return k,index,x


def G_S(A,b,x0,it_max,ep):
    D=np.diag(np.diag(A))
    U=-np.triu(A,1)
    L=-np.tril(A,-1)
    B=np.dot (np.linalg.inv (D-L),U)
    f=np.dot (np.linalg.inv(D-L),b)
    x=np.dot(B,x0)+f
    k=1
    index=1
    while np.linalg.norm(x-x0)>=ep:
        x=x0
        x=np.dot(B,x0)+f
        k=k+1
        if k>it_max:
            index=0
            break
        return k,index,x


A=[[4,3,0],[3,4,-1],[0,-1,4]]
b=[24,30,-24]
x0=[0,0,0]
print(Jacobi(A,b,x0,100,0.000001))
print(G_S(A,b,x0,100,0.000001))

