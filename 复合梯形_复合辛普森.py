# -*- coding: utf-8 -*-
"""
Created on Fri Apr 15 09:25:39 2022

@author: Administrator
"""

#被积函数
def fun (x) :
    return 1/ x
#复合梯形
def tx (a,b,n) :
    h= (b-a) /n
    x=a
    s=fun (x) -fun (b)
    for k in range (1,n+1) :
        x=x+h
        s=s+2*fun (x)
        result= (h/2) *s
        return result
#复合辛普森
def xps (a,b,n) :
    h= (b-a) /n
    x=a
    s=fun (x) -fun (b)
    for k in range (1,n+1) :
        x=x+h/2
        s=s+4*fun (x)
        x=x+h/2
        s=s+2*fun (x)
    result= (h/6) *s
    return result
a=1
b=5
n1=40
n2=20
t=tx (a,b,n1)
p=xps (a,b,n2)
print ("梯形公式：{}\n辛普森公式：{}".format(t,p)) 